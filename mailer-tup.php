<?php
    /**
     * PHPMailer multiple files upload and send
     */

    //Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    require './PHPMailer/PHPMailer.php';
    require './PHPMailer/SMTP.php';
    require './PHPMailer/Exception.php';

    //Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
    $mail->CharSet = 'UTF-8';

    //Server settings
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'in-v3.mailjet.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'b69ce8774a813b63fead8fc260943ede';                     //SMTP username
    $mail->Password   = '3123f0598c45e6b2a2999dbe7a16d0f4';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;
    // $mail->SMTPDebug = 1;

    if (isset($_FILES['tup-userfile']['tmp_name'])) {
        $name = strip_tags(trim($_POST["tup-nombre"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["tup-correo"]), FILTER_SANITIZE_EMAIL);
        $phone = trim($_POST["tup-telefono"]);
        $message = trim($_POST["tup-mensaje"]);

        try {
            //Recipients
            $mail->setFrom($email, $name);
            $mail->addAddress('info@demsaindustrial.com');     //Add a recipient
            // $mail->addAddress('');     //Add extra recipient
            $mail->addReplyTo($email, 'Hola, tengo un proyecto en mente.');

            //Attachments
            //Attach multiple files one by one
            if (isset($_FILES['tup-userfile']['tmp_name'])) {
                foreach ($_FILES["tup-userfile"]["name"] as $k => $v) {
                    $mail->AddAttachment( $_FILES["tup-userfile"]["tmp_name"][$k], $_FILES["tup-userfile"]["name"][$k] );
                }
            }

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'Hola, tengo un proyecto en mente.';
            $mail->Body    = 'Nombre: ' . $name . '<br>Correo electrónico: ' . $email . '<br>Teléfono: ' . $phone . '<br><br>Mensaje:<br>' . $message . '<br><br>Este mensaje fue enviado a través de un formulario de contacto del sitio web DEMSA Industrial.';

            $mail->send();
            echo 'Gracias por contactarnos, nos comunicaremos contigo a la brevedad.';
        } catch (Exception $e) {
            echo 'Lo sentimos, algo salió mal. Por favor, inténtalo de nuevo. Mailer Error: {$mail->ErrorInfo}';
        }
    }
?>